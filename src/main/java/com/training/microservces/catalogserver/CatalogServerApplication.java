package com.training.microservces.catalogserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class CatalogServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogServerApplication.class, args);
	}

}
